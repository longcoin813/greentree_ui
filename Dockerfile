#Script1
FROM maven:3.6.3-jdk-8 as MAVEN_BUILD

USER root

WORKDIR /build/

COPY src /build/src/

COPY pom.xml /build/

RUN mvn install


#script2
FROM adoptopenjdk/openjdk8:alpine-jre

USER root

#WORKDIR /app/- docker run -d -p 80:8080 -

COPY --from=MAVEN_BUILD /build/target/GreenTree-0.0.1-SNAPSHOT.jar .

EXPOSE 3000

VOLUME /tmp

#ADD /build/target/GreenTree-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java", "-jar"]

CMD ["GreenTree-0.0.1-SNAPSHOT.jar"]