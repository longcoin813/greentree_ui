/*Dashboard Init*/

"use strict";

/*****Ready function start*****/
$(document).ready(function () {
	"use strict";
	$('#timesetting').datetimepicker({
		format: 'LT',
		useCurrent: false,
		icons: {
			time: "fa fa-clock-o",
			date: "fa fa-calendar",
			up: "fa fa-arrow-up",
			down: "fa fa-arrow-down"
		},
	}).data("DateTimePicker").date(moment());
});
/*****Ready function end*****/
