/*Dashboard Init*/

"use strict";

/*****Ready function start*****/
$(document).ready(function () {
	"use strict";
	if ($('#chart_tilt').length > 0) {
		var ctx1 = document.getElementById("chart_tilt").getContext("2d");
		var data1 = {
			labels: ["08-01", "08-02", "08-03", "08-04", "08-05", "08-06", "08-07", "08-08", "08-09", "08-10", "08-11", "08-12", 
			"08-13", "08-14", "08-15", "08-16", "08-17", "08-18", "08-19", "08-20", "08-21", "08-22", "08-23", "08-24", "08-25",
			"08-26", "08-27", "08-28", "08-29", "08-30", "08-31", "09-01"],
			datasets: [{
					label: "cây",
					backgroundColor: "rgba(60,184,120,0.4)",
					borderColor: "rgba(60,184,120,0.4)",
					pointBorderColor: "rgb(60,184,120)",
					pointHighlightStroke: "rgba(60,184,120,1)",
					data: [0, 59, 80, 58, 20, 55, 40, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 55, 55, 55, 55, 55, 55]
				}
			]
		};

		var areaChart = new Chart(ctx1, {
			type: "line",
			data: data1,

			options: {
				tooltips: {
					mode: "label"
				},
				elements: {
					point: {
						hitRadius: 90
					}
				},

				scales: {
					yAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}],
					xAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}]
				},
				animation: {
					duration: 3000
				},
				responsive: true,
				legend: {
					display: false,
				},
				tooltip: {
					backgroundColor: 'rgba(47,44,44,.9)',
					cornerRadius: 0,
					footerFontFamily: "'Varela Round'"
				}

			}
		});
	}

	if ($('#chart_tilt_direction').length > 0) {
		var ctx1 = document.getElementById("chart_tilt_direction").getContext("2d");
		var data1 = {
			labels: ["08-01", "08-02", "08-03", "08-04", "08-05", "08-06", "08-07", "08-08", "08-09", "08-10", "08-11", "08-12",
				"08-13", "08-14", "08-15", "08-16", "08-17", "08-18", "08-19", "08-20", "08-21", "08-22", "08-23", "08-24", "08-25",
				"08-26", "08-27", "08-28", "08-29", "08-30", "08-31", "09-01"
			],
			datasets: [{
				label: "cây",
				backgroundColor: "rgba(60,104,184,0.4)",
				borderColor: "rgba(60,104,184,1)",
				pointBorderColor: "rgba(60,104,184,1)",
				pointHighlightStroke: "rgba(60,104,184,1)",
				data: [20, 20, 20, 20, 20, 20, 20, 20, 35, 59, 80, 58, 20, 55, 40, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 55, 55, 55, 55, 55, 55]
			}]
		};

		var areaChart = new Chart(ctx1, {
			type: "line",
			data: data1,

			options: {
				tooltips: {
					mode: "label"
				},
				elements: {
					point: {
						hitRadius: 90
					}
				},

				scales: {
					yAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}],
					xAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}]
				},
				animation: {
					duration: 3000
				},
				responsive: true,
				legend: {
					display: false,
				},
				tooltip: {
					backgroundColor: 'rgba(47,44,44,.9)',
					cornerRadius: 0,
					footerFontFamily: "'Varela Round'"
				}

			}
		});
	}

	if ($('#chart_battery_voltage').length > 0) {
		var ctx1 = document.getElementById("chart_battery_voltage").getContext("2d");
		var data1 = {
			labels: ["08-01", "08-02", "08-03", "08-04", "08-05", "08-06", "08-07", "08-08", "08-09", "08-10", "08-11", "08-12",
				"08-13", "08-14", "08-15", "08-16", "08-17", "08-18", "08-19", "08-20", "08-21", "08-22", "08-23", "08-24", "08-25",
				"08-26", "08-27", "08-28", "08-29", "08-30", "08-31", "09-01"
			],
			datasets: [{
				label: "cây",
				backgroundColor: "rgba(232,160,29,0.4)",
				borderColor: "rgba(232,160,29,0.4)",
				pointBorderColor: "rgb(232,160,29)",
				pointHighlightStroke: "rgba(232,160,29,1)",
				data: [55, 40, 20, 20, 20, 20, 20, 20, 33, 59, 80, 58, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 55, 55, 33, 59, 80, 58]
			}]
		};

		var areaChart = new Chart(ctx1, {
			type: "line",
			data: data1,

			options: {
				tooltips: {
					mode: "label"
				},
				elements: {
					point: {
						hitRadius: 90
					}
				},

				scales: {
					yAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}],
					xAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}]
				},
				animation: {
					duration: 3000
				},
				responsive: true,
				legend: {
					display: false,
				},
				tooltip: {
					backgroundColor: 'rgba(47,44,44,.9)',
					cornerRadius: 0,
					footerFontFamily: "'Varela Round'"
				}

			}
		});
	}

	if ($('#chart_temperature').length > 0) {
		var ctx1 = document.getElementById("chart_temperature").getContext("2d");
		var data1 = {
			labels: ["08-01", "08-02", "08-03", "08-04", "08-05", "08-06", "08-07", "08-08", "08-09", "08-10", "08-11", "08-12",
				"08-13", "08-14", "08-15", "08-16", "08-17", "08-18", "08-19", "08-20", "08-21", "08-22", "08-23", "08-24", "08-25",
				"08-26", "08-27", "08-28", "08-29", "08-30", "08-31", "09-01"
			],
			datasets: [{
				label: "cây",
				backgroundColor: "rgba(224,36,36,0.4)",
				borderColor: "rgba(224,36,36,0.4)",
				pointBorderColor: "rgb(224,36,36)",
				pointHighlightStroke: "rgba(224,36,36,1)",
				data: [20, 55, 40, 20, 20, 20, 20, 66, 59, 80, 58, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 55, 20, 20, 20, 20, 20]
			}]
		};

		var areaChart = new Chart(ctx1, {
			type: "line",
			data: data1,

			options: {
				tooltips: {
					mode: "label"
				},
				elements: {
					point: {
						hitRadius: 90
					}
				},

				scales: {
					yAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}],
					xAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}]
				},
				animation: {
					duration: 3000
				},
				responsive: true,
				legend: {
					display: false,
				},
				tooltip: {
					backgroundColor: 'rgba(47,44,44,.9)',
					cornerRadius: 0,
					footerFontFamily: "'Varela Round'"
				}

			}
		});
	}
	if ($('#chart_humidity').length > 0) {
		var ctx1 = document.getElementById("chart_humidity").getContext("2d");
		var data1 = {
			labels: ["08-01", "08-02", "08-03", "08-04", "08-05", "08-06", "08-07", "08-08", "08-09", "08-10", "08-11", "08-12",
				"08-13", "08-14", "08-15", "08-16", "08-17", "08-18", "08-19", "08-20", "08-21", "08-22", "08-23", "08-24", "08-25",
				"08-26", "08-27", "08-28", "08-29", "08-30", "08-31", "09-01"
			],
			datasets: [{
				label: "cây",
				backgroundColor: "rgba(74,199,247,0.4)",
				borderColor: "rgba(74,199,247,0.4)",
				pointBorderColor: "rgb(74,199,247)",
				pointHighlightStroke: "rgba(74,199,247,1)",
				data: [20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 59, 59, 80, 58, 20, 55, 40, 20, 20, 20, 20, 20, 20, 55, 55, 55, 55, 55, 55]
			}]
		};

		var areaChart = new Chart(ctx1, {
			type: "line",
			data: data1,

			options: {
				tooltips: {
					mode: "label"
				},
				elements: {
					point: {
						hitRadius: 90
					}
				},

				scales: {
					yAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}],
					xAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}]
				},
				animation: {
					duration: 3000
				},
				responsive: true,
				legend: {
					display: false,
				},
				tooltip: {
					backgroundColor: 'rgba(47,44,44,.9)',
					cornerRadius: 0,
					footerFontFamily: "'Varela Round'"
				}

			}
		});
	}
	if ($('#chart_co2').length > 0) {
		var ctx1 = document.getElementById("chart_co2").getContext("2d");
		var data1 = {
			labels: ["08-01", "08-02", "08-03", "08-04", "08-05", "08-06", "08-07", "08-08", "08-09", "08-10", "08-11", "08-12",
				"08-13", "08-14", "08-15", "08-16", "08-17", "08-18", "08-19", "08-20", "08-21", "08-22", "08-23", "08-24", "08-25",
				"08-26", "08-27", "08-28", "08-29", "08-30", "08-31", "09-01"
			],
			datasets: [{
				label: "cây",
				backgroundColor: "rgba(221,221,221,0.4)",
				borderColor: "rgba(221,221,221,0.4)",
				pointBorderColor: "rgb(221,221,221)",
				pointHighlightStroke: "rgba(221,221,221,1)",
				data: [20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 55, 55, 55, 55, 55, 55, 44, 59, 80, 58, 20, 55, 40, 20, 20, 20, 20, 20, 20, 20, 20]
			}]
		};

		var areaChart = new Chart(ctx1, {
			type: "line",
			data: data1,

			options: {
				tooltips: {
					mode: "label"
				},
				elements: {
					point: {
						hitRadius: 90
					}
				},

				scales: {
					yAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}],
					xAxes: [{
						stacked: true,
						gridLines: {
							color: "#eee",
						},
						ticks: {
							fontFamily: "Varela Round",
							fontColor: "#2f2c2c"
						}
					}]
				},
				animation: {
					duration: 3000
				},
				responsive: true,
				legend: {
					display: false,
				},
				tooltip: {
					backgroundColor: 'rgba(47,44,44,.9)',
					cornerRadius: 0,
					footerFontFamily: "'Varela Round'"
				}

			}
		});
	}

	/* Daterange picker Init*/
	$('.input-daterange-datepicker').daterangepicker({
		buttonClasses: ['btn', 'btn-sm'],
		applyClass: 'btn-info',
		cancelClass: 'btn-default'
	});
});
/*****Ready function end*****/
