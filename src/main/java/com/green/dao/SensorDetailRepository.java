package com.green.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.green.model.SensorDetail;

public interface SensorDetailRepository extends JpaRepository<SensorDetail, Long>{

}
