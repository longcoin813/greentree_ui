package com.green.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.green.model.Tree;

public interface TreeRepository extends JpaRepository<Tree, Long> {

}
