package com.green.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.green.model.TreeDetail;

public interface TreeDetailRepository extends JpaRepository<TreeDetail, Long> {

}
