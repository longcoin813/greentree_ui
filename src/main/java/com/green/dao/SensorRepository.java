package com.green.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.green.model.Sensor;

public interface SensorRepository extends JpaRepository<Sensor, Long>{

}
