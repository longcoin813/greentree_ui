package com.green.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.green.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
