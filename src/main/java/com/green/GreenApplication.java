package com.green;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.green.manager.serverRunner.ServerRunner;



@SpringBootApplication
public class GreenApplication {

	public static void main(String[] args) throws Exception {
		ApplicationContext app = SpringApplication.run(GreenApplication.class, args);
		ServerRunner runner = new ServerRunner();
		runner.server();
	}
}
