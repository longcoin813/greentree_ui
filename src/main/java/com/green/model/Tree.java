package com.green.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.green.manager.tack.TreeDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "tree")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Tree implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "treeid")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long treeId;

	@Column(name = "treename")
	private String treeName;

	@Column(name = "treelocation")
	private String treeLocation;

	@Column(name = "treearea")
	private String treeArea;

	@Column(name = "treestatus")
	private Integer treeStatus;

	@OneToMany(mappedBy = "tree")
	private List<TreeDetail> treeDetail = new ArrayList<>();
}
