package com.green.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "treedetail")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class TreeDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "treedetailid")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long treeDetailId;

	@Column(name = "treeage")
	private Double treeAge;

	@Column(name = "ground")
	private String ground;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy@HH:mm:ss.SSSZ") 
	@Column(name = "treecaretimes", columnDefinition = "TIMESTAMP")
	private Date treeCareTimes;

	@Column(name = "treeStatus")
	private Integer treeStatus;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "treeid")
	@JsonIgnore
	private Tree tree;

}
