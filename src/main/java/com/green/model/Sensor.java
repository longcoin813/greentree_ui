package com.green.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "sensor")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Sensor implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "sensorid")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long sensorId;
	
	@OneToOne
	@JoinColumn(name = "treeid")
	@JsonIgnore
	private Tree tree;
	
	@OneToMany(mappedBy = "sensor")
	@JsonIgnore
	private List<SensorDetail> sensorDetail = new ArrayList<>();
	
	@Column(name = "locationsensor")
	private String locationSensor;
	
	@Column(name = "sensorname")
	private String sensorName;
	
	@Column(name = "samplingspeed")
	private Double samplingSpeed;
}
