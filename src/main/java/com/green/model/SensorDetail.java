package com.green.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "sensordetail")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class SensorDetail implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "sensordetailid")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long sensorDetailId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name = "sensorid")
	private Sensor sensor;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy@HH:mm:ss.SSSZ") 
	@Column(name = "day", columnDefinition = "TIMESTAMP")
	private Date day;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "tilt")
	private Double tilt;
	
	@Column(name = "tiltanglethreshold")
	private Double tiltAngleThreshold;
	
	@Column(name = "inclinationdirection")
	private Double inclinationDirection;
	
	@Column(name = "batteryvoltage")
	private Double batteryVoltage;
	
	@Column(name = "temperature")
	private Double temperature;
	
	@Column(name = "humidity")
	private Double humidity;
	
	@Column(name = "co2")
	private Double co2;
}
