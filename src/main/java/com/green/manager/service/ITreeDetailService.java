package com.green.manager.service;

import java.util.List;

import com.green.model.TreeDetail;

public interface ITreeDetailService {
	List<TreeDetail> findAll(TreeDetail tree);

	TreeDetail save(TreeDetail tree);

	TreeDetail update(TreeDetail tree);

	void remove(TreeDetail tree);
}
