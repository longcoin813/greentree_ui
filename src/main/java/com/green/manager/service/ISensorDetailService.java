package com.green.manager.service;

import java.util.List;

import com.green.model.SensorDetail;

public interface ISensorDetailService {
	List<SensorDetail> findAll(SensorDetail sensor);
	SensorDetail save(SensorDetail sensor);
	SensorDetail update(SensorDetail sensor);
	void remove(SensorDetail sensor);
}
