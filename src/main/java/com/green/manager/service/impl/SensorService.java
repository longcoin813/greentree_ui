package com.green.manager.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.green.dao.SensorRepository;
import com.green.manager.service.ISensorService;
import com.green.model.Sensor;

@Service
@Transactional(rollbackOn = Throwable.class)
public class SensorService implements ISensorService {
	
	@Autowired
	private SensorRepository sensorRepository;
	
	@Override
	public List<Sensor> findAll(Sensor sensor) {
		List<Sensor> sensorEntity = sensorRepository.findAll();
		return sensorEntity;
	}	
	
	@Override
	public Sensor save(Sensor sensor) {
		return sensorRepository.save(sensor);
	}

	@Override
	public Sensor update(Sensor sensor) {
		return sensorRepository.save(sensor);
	}

	@Override
	public void remove(Sensor sensor) {
		sensorRepository.delete(sensor);
	}
}
