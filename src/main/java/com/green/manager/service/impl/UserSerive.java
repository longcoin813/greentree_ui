package com.green.manager.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.green.dao.UserRepository;
import com.green.manager.service.ISensorDetailService;
import com.green.manager.service.IUserService;
import com.green.model.User;

@Service
@Transactional(rollbackOn = Throwable.class)
public class UserSerive implements IUserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<User> findAll(User user) {
		List<User> userEntity = userRepository.findAll();
		return userEntity;
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public Optional<User> findById(Long userId) {
		return userRepository.findById(userId);
	}

	@Override
	public User update(User user) {
		return userRepository.save(user);
	}

	@Override
	public void remove(User user) {
		userRepository.delete(user);

	}

}
