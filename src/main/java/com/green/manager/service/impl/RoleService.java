package com.green.manager.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.green.dao.RoleRepository;
import com.green.manager.service.IRoleService;
import com.green.model.Role;

@Service
@Transactional(rollbackOn = Throwable.class)
public class RoleService implements IRoleService {
	@Autowired
	private RoleRepository roleRepository;

	@Override
	public List<Role> findAll(Role role) {
		List<Role> roleEntity = roleRepository.findAll();
		return roleEntity;
	}

	@Override
	public Role save(Role role) {
		// TODO Auto-generated method stub
		return roleRepository.save(role);
	}

	@Override
	public Role update(Role role) {
		// TODO Auto-generated method stub
		return roleRepository.save(role);
	}

	@Override
	public void remove(Role role) {
		// TODO Auto-generated method stub
		roleRepository.delete(role);
	}

}
