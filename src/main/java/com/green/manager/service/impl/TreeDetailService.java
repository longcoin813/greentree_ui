package com.green.manager.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.green.dao.TreeDetailRepository;
import com.green.manager.service.ITreeDetailService;
import com.green.model.TreeDetail;

@Service
@Transactional(rollbackOn = Throwable.class)
public class TreeDetailService implements ITreeDetailService {

	@Autowired
	private TreeDetailRepository treeDetailRepository;

	@Override
	public List<TreeDetail> findAll(TreeDetail treeDetail) {
		List<TreeDetail> treeDetailEntity = treeDetailRepository.findAll();
		return treeDetailEntity;
	}

	@Override
	public TreeDetail save(TreeDetail treeDetail) {
		// TODO Auto-generated method stub
		return treeDetailRepository.save(treeDetail);
	}

	@Override
	public TreeDetail update(TreeDetail treeDetail) {
		// TODO Auto-generated method stub
		return treeDetailRepository.save(treeDetail);
	}

	@Override
	public void remove(TreeDetail treeDetail) {
		// TODO Auto-generated method stub
		treeDetailRepository.delete(treeDetail);

	}

}
