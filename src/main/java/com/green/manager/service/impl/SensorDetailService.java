package com.green.manager.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.green.dao.SensorDetailRepository;
import com.green.manager.service.ISensorDetailService;
import com.green.model.SensorDetail;

@Service
@Transactional(rollbackOn = Throwable.class)
public class SensorDetailService implements ISensorDetailService{
	
	@Autowired
	private SensorDetailRepository sensorDetailRepository;
	
	@Override
	public List<SensorDetail> findAll(SensorDetail sensorDetail) {
		List<SensorDetail> sensorDetailEntity = sensorDetailRepository.findAll();
		return sensorDetailEntity;
	}
 
	@Override
	public SensorDetail save(SensorDetail sensorDetail) {
		return sensorDetailRepository.save(sensorDetail);
	}

	@Override
	public SensorDetail update(SensorDetail sensorDetail) {
		return sensorDetailRepository.save(sensorDetail);
	}

	@Override
	public void remove(SensorDetail sensorDetail) {
		sensorDetailRepository.delete(sensorDetail);
	}

}
