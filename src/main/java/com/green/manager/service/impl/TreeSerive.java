package com.green.manager.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.green.dao.TreeRepository;
import com.green.manager.service.ItreeService;
import com.green.model.Tree;

@Service
@Transactional(rollbackOn = Throwable.class)
public class TreeSerive implements ItreeService {

	@Autowired
	private TreeRepository treeRepository;

	@Override
	public List<Tree> findAll(Tree tree) {
		List<Tree> treeEntity = treeRepository.findAll();
		return treeEntity;
	}

	@Override
	public Tree save(Tree tree) {
		// TODO Auto-generated method stub
		return treeRepository.save(tree);
	}

	@Override
	public Tree update(Tree tree) {
		// TODO Auto-generated method stub
		return treeRepository.save(tree);
	}

	@Override
	public void remove(Tree tree) {
		// TODO Auto-generated method stub
		treeRepository.delete(tree);
	}

	@Override
	public Optional<Tree> findById(Long treeId) {
		// TODO Auto-generated method stub
		return treeRepository.findById(treeId);
	}

}
