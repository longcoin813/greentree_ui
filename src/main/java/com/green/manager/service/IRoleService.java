package com.green.manager.service;

import java.util.List;

import com.green.model.Role;

public interface IRoleService {

	List<Role> findAll(Role role);

	Role save(Role role);

	Role update(Role role);

	void remove(Role role);

}
