package com.green.manager.service;

import java.util.List;
import java.util.Optional;

import com.green.model.Tree;

public interface ItreeService {

	List<Tree> findAll(Tree tree);

	Tree save(Tree tree);

	Tree update(Tree tree);

	void remove(Tree tree);

	Optional<Tree> findById(Long treeId);
}
