package com.green.manager.service;

import java.util.List;
import java.util.Optional;

import com.green.model.User;

public interface IUserService {

	List<User> findAll(User user);

	User save(User user);

	User update(User user);

	void remove(User user);

	Optional<User> findById(Long userId);
}
