package com.green.manager.service;

import java.util.List;

import com.green.model.Sensor;

public interface ISensorService {
	List<Sensor> findAll(Sensor sensor);
	Sensor save(Sensor sensor);
	Sensor update(Sensor sensor);
	void remove(Sensor sensor);
}