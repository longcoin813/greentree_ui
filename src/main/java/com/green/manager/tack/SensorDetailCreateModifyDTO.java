package com.green.manager.tack;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class SensorDetailCreateModifyDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long sensorDetailId;
	private Long sensorId;
	private Date day;
	private String status;
	private Double tilt;
	private Double tiltAngleThreshold;
	private Double inclinationDirection;
	private Double batteryVoltage;
	private Double temperature;
	private Double humidity;
	private Double co2;
}

