package com.green.manager.tack;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class SensorCreateModifyDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long sensorId;
	private Long treeId;
	private String locationSensor;
	private String sensorName;
	private Double samplingSpeed;
	
}
