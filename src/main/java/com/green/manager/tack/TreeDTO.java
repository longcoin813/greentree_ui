package com.green.manager.tack;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.green.model.Tree.TreeBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString

public class TreeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long treeId;
	private String treeName;
	private String treeLocation;
	private String treeArea;
	private Integer treeStatus;

}
