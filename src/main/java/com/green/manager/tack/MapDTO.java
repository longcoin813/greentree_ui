package com.green.manager.tack;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.green.model.Tree;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "map")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class MapDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "mapid")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long mapId;
	
	@OneToOne
	@JoinColumn(name = "treeid")
	private Tree treeId;
	
	@Column
	private String coordinates; 
}
