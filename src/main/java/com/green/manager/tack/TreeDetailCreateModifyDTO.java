package com.green.manager.tack;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class TreeDetailCreateModifyDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long treeDetailId;

	private Double treeAge;

	private String ground;

	private Date treeCareTimes;

	private Integer treeStatus;

	private Long treeId;
}
