package com.green.manager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/sensor")
	public String index(Model model) {
		return "sensor";
	}

	@GetMapping("/")
	public String login() {
		return "login";
	}

	@GetMapping("/managertree")
	public String managertree() {
		return "managertree";
	}

	@GetMapping("/chartsensor")
	public String chartsensor() {
		return "chartsensor";
	}

	@GetMapping("/manageraccount")
	public String manageraccount() {
		return "manageraccount";
	}

	@GetMapping("/managerauthorize")
	public String managerauthorize() {
		return "managerauthorize";
	}

	@GetMapping("/managerdevice")
	public String managerdevice() {
		return "managerdevice";
	}

	@GetMapping("/managertreemap")
	public String managertreemap() {
		return "managertreemap";
	}

	@GetMapping("/sensor_detail")
	public String sensordetail() {
		return "sensor_detail";
	}
	
	@GetMapping("/singup-page")
	public String singuppage() {
		return "singup-page";
	}
	
	@GetMapping("/takecareplant.")
	public String takecareplant() {
		return "takecareplant";
	}
	@GetMapping("/testExcel")
	public String testExcell() {
		return "testExcel";
	}
	
	@GetMapping("/user")
	public String user() {
		return "user";
	}
	
}
