package com.green.manager.API;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.green.dao.RoleRepository;
import com.green.manager.converter.RoleConverter;
import com.green.manager.service.IRoleService;
import com.green.manager.tack.RoleDTO;
import com.green.model.Role;

@RestController
public class RoleAPI {
	@Autowired
	private RoleConverter roleConverter;

	@Autowired
	private IRoleService roleService;

	@Autowired
	private RoleRepository roleRepository;

	@GetMapping("/api/Role")
	public List<Role> findAllRole() {
		List<Role> role = null;
		return roleService.findAll((Role) role);
	}

	@PostMapping("/api/Role")
	public ResponseEntity<RoleDTO> createRole(@RequestBody RoleDTO dto) {
		Role role = roleConverter.toRole(dto);
		return ResponseEntity.ok(roleConverter.toRoleResponseDTO(roleService.save(role)));
	}

	@PutMapping("/api/Role/{roleId}")
	public Role updateRole(@RequestBody RoleDTO dto, @PathVariable("roleId") long roleId) {
		Role role = new Role();
		dto.setRoleId(roleId);
		Role old = roleRepository.findById(dto.getRoleId()).orElse(null);
		role = roleConverter.toRole(dto, old);
		return roleService.update(role);
	}

	@DeleteMapping("/api/Role/{roleId}")
	public ResponseEntity<Role> deleteRole(@PathVariable("roleId") long id) {
		Role ids = roleRepository.findById(id).orElse(null);
		roleService.remove(ids);
		return ResponseEntity.ok().build();
	}
}
