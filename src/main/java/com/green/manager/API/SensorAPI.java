package com.green.manager.API;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.green.dao.SensorRepository;
import com.green.manager.converter.SensorConverter;
import com.green.manager.service.ISensorService;
import com.green.manager.tack.SensorCreateModifyDTO;
import com.green.model.Sensor;
import com.green.model.SensorDetail;

@RestController
public class SensorAPI {

	@Autowired
	private SensorConverter sensorConverter;

	@Autowired
	private ISensorService sensorService;

	@Autowired
	private SensorRepository sensorRepository;

	@GetMapping("/api/Sensor")
	public List<Sensor> findAllSensor() {
		List<Sensor> sensor = null;
		return sensorService.findAll((Sensor) sensor);
	}

	@PostMapping("/api/Sensor")
	public ResponseEntity<SensorCreateModifyDTO> createSensor(@RequestBody SensorCreateModifyDTO dto) {
		Sensor sensor = sensorConverter.toSensor(dto);
		return ResponseEntity.ok(sensorConverter.toSensorResponseDTO(sensorService.save(sensor)));
	}

	@PutMapping("/api/Sensor/{sensorId}")
	public Sensor updateSensor(@RequestBody SensorCreateModifyDTO dto, @PathVariable("sensorId") long sensorId) {
		Sensor sensor = new Sensor();
		dto.setSensorId(sensorId);
		Sensor old = sensorRepository.findById(dto.getSensorId()).orElse(null);
		sensor = sensorConverter.toSensor(dto, old);
		return sensorService.update(sensor);
	}

	@DeleteMapping("/api/Sensor/{sensorId}")
	public ResponseEntity<Sensor> deleteSensor(@PathVariable("sensorId") long id) {
		Sensor ids = sensorRepository.findById(id).orElse(null);
		sensorService.remove(ids);
		return ResponseEntity.ok().build();
	}

}



