package com.green.manager.API;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.green.dao.TreeRepository;
import com.green.manager.converter.TreeConverter;
import com.green.manager.service.ItreeService;
import com.green.manager.tack.TreeDTO;
import com.green.model.Tree;
import com.green.model.User;

@RestController

public class TreeAPI {

	@Autowired
	private TreeConverter treeConverter;

	@Autowired
	private ItreeService treeService;

	@Autowired
	private TreeRepository treeRepository;

	@GetMapping("/api/Tree")
	public List<Tree> findAllTree() {
		List<Tree> tree = null;
		return treeService.findAll((Tree) tree);
	}

	@GetMapping("/api/Tree/{treeId}")
	public Optional<Tree> findByid(@PathVariable("treeId") Long treeId) {
		return treeService.findById(treeId);
	}

	@PostMapping("/api/Tree")
	public ResponseEntity<TreeDTO> createTree(@RequestBody TreeDTO dto) {
		Tree tree = treeConverter.toTree(dto);
		return ResponseEntity.ok(treeConverter.toTreeResponseDTO(treeService.save(tree)));
	}

	@PutMapping("/api/Tree/{treeId}")
	public Tree updateTree(@RequestBody TreeDTO dto, @PathVariable("treeId") long treeId) {
		Tree tree = new Tree();
		dto.setTreeId(treeId);
		Tree old = treeRepository.findById(dto.getTreeId()).orElse(null);
		tree = treeConverter.toTree(dto, old);
		return treeService.update(tree);
	}

	@DeleteMapping("/api/Tree/{treeId}")
	public ResponseEntity<Tree> deleteTree(@PathVariable("treeId") long id) {
		Tree ids = treeRepository.findById(id).orElse(null);
		treeService.remove(ids);
		return ResponseEntity.ok().build();
	}

}
