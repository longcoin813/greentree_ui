package com.green.manager.API;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.green.dao.SensorDetailRepository;
import com.green.manager.converter.SensorDetailConverter;
import com.green.manager.service.impl.SensorDetailService;
import com.green.manager.tack.SensorDetailCreateModifyDTO;
import com.green.model.Sensor;
import com.green.model.SensorDetail;

@RestController
public class SensorDetailAPI {
	@Autowired
	private SensorDetailService sensorDetailService;

	@Autowired
	private SensorDetailConverter sensorDetailConverter;

	@Autowired
	private SensorDetailRepository sensorDetailRepository;

	@GetMapping("/api/SensorDetail")
	public List<SensorDetail> findAllSensorDetail() {
		List<SensorDetail> sensorDetail = null;
		return sensorDetailService.findAll((SensorDetail) sensorDetail);
	}

	@PostMapping("/api/SensorDetail")
	public ResponseEntity<SensorDetailCreateModifyDTO> createSensorDetail(
			@RequestBody SensorDetailCreateModifyDTO dto) {
		SensorDetail sensorDetail = sensorDetailConverter.toSensorDetail(dto);
		System.out.println("sensorDetail: " + sensorDetail);
		return ResponseEntity
				.ok(sensorDetailConverter.toSensorDetailResponseDTO(sensorDetailService.save(sensorDetail)));
	}

	@PutMapping("/api/SensorDetail/{sensorDetailId}")
	public SensorDetail updateSensor(@RequestBody SensorDetailCreateModifyDTO dto,
			@PathVariable("sensorDetailId") long sensorDetailId) {
		SensorDetail sensorDetail = new SensorDetail();
		dto.setSensorDetailId(sensorDetailId);
		SensorDetail old = sensorDetailRepository.findById(dto.getSensorDetailId()).orElse(null);
		sensorDetail = sensorDetailConverter.toSensorDetail(dto, old);
		return sensorDetailService.update(sensorDetail);
	}

	@DeleteMapping("/api/SensorDetail/{sensorDetailId}")
	public ResponseEntity<SensorDetail> deleteSensorDetail(@PathVariable("sensorDetailId") long id) {
		SensorDetail ids = sensorDetailRepository.findById(id).orElse(null);
		sensorDetailService.remove(ids);
		return ResponseEntity.ok().build();
	}
}
