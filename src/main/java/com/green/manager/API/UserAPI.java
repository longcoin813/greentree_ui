package com.green.manager.API;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.green.dao.UserRepository;
import com.green.manager.converter.UserConverter;
import com.green.manager.service.IUserService;
import com.green.manager.tack.UserDTO;
import com.green.model.User;

@RestController
public class UserAPI {

	@Autowired
	private UserConverter userConverter;

	@Autowired
	private IUserService userService;

	@Autowired
	private UserRepository userRepository;

	@GetMapping("/api/User")
	public List<User> findAllUser() {
		List<User> user = null;
		return userService.findAll((User) user);
	}

	@GetMapping("/api/User/{userId}")
	public Optional<User> findByid(@PathVariable("userId") Long userId) {
		return userService.findById(userId);
	}

	@PostMapping("/api/User")
	public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO dto) {
		User user = userConverter.toUser(dto);
		return ResponseEntity.ok(userConverter.toUserResponseDTO(userService.save(user)));
	}

	@PutMapping("/api/User/{userId}")
	public User updateUser(@RequestBody UserDTO dto, @PathVariable("userId") long userId) {
		User user = new User();
		dto.setUserId(userId);
		User old = userRepository.findById(dto.getUserId()).orElse(null);
		user = userConverter.toUser(dto, old);
		return userService.update(user);
	}

	@DeleteMapping("/api/User/{userId}")
	public ResponseEntity<User> deleteUser(@PathVariable("userId") long id) {
		User ids = userRepository.findById(id).orElse(null);
		userService.remove(ids);
		return ResponseEntity.ok().build();
	}
}
