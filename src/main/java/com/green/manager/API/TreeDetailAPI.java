package com.green.manager.API;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.green.dao.TreeDetailRepository;
import com.green.manager.converter.TreeDetailConverter;
import com.green.manager.service.ITreeDetailService;
import com.green.manager.tack.TreeDetailCreateModifyDTO;
import com.green.model.TreeDetail;

@RestController
public class TreeDetailAPI {

	@Autowired
	private TreeDetailConverter treeDetailConverter;

	@Autowired
	private ITreeDetailService treeDetailService;

	@Autowired
	private TreeDetailRepository treeDetailRepository;

	@GetMapping("/api/TreeDetail")
	public List<TreeDetail> findAllTreeDetail() {
		List<TreeDetail> treeDetail = null;
		return treeDetailService.findAll((TreeDetail) treeDetail);
	}

	@PostMapping("/api/TreeDetail")
	public ResponseEntity<TreeDetailCreateModifyDTO> createTreeDetail(@RequestBody TreeDetailCreateModifyDTO dto) {
		TreeDetail treeDetail = treeDetailConverter.toTreeDetail(dto);
		System.out.print("treedetailAPI: " + treeDetail);
		return ResponseEntity.ok(treeDetailConverter.toTreeDetailResponseDTO(treeDetailService.save(treeDetail)));
	}

	@PutMapping("/api/TreeDetail/{treeDetailId}")
	public TreeDetail updateTreeDetail(@RequestBody TreeDetailCreateModifyDTO dto,
			@PathVariable("treeDetailId") long treeDetailId) {
		TreeDetail treeDetail = new TreeDetail();
		dto.setTreeDetailId(treeDetailId);
		TreeDetail old = treeDetailRepository.findById(dto.getTreeDetailId()).orElse(null);
		treeDetail = treeDetailConverter.toTreeDetail(dto, old);
		return treeDetailService.update(treeDetail);
	}

	@DeleteMapping("/api/TreeDetail/{treeDetailId}")
	public ResponseEntity<TreeDetail> deleteTreeDetail(@PathVariable("treeDetailId") long id) {
		TreeDetail ids = treeDetailRepository.findById(id).orElse(null);
		treeDetailService.remove(ids);
		return ResponseEntity.ok().build();
	}

}
