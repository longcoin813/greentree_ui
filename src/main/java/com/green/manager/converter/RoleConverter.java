package com.green.manager.converter;

import org.springframework.stereotype.Component;

import com.green.manager.tack.RoleDTO;
import com.green.model.Role;

@Component
public class RoleConverter {
	public Role toRole(RoleDTO dto) {

		Role role = Role.builder().roleId(dto.getRoleId()).build();

		return Role.builder().roleName(dto.getRoleName())
				.code(dto.getCode())
				.build();

	}

	public RoleDTO toRoleResponseDTO(Role role) {
		RoleDTO dto = RoleDTO.builder().roleId(role.getRoleId() != null ? role.getRoleId() : null)
				.roleName(role.getRoleName())
				.code(role.getCode())
				.build();
				
		return dto;

	}

	public Role toRole(RoleDTO dto, final Role role) {
		role.setRoleName(dto.getRoleName());
		role.setCode(dto.getCode());
		System.out.println("to update");
		return role;
	}
}
