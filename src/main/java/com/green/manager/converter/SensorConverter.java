package com.green.manager.converter;

import org.springframework.stereotype.Component;

import com.green.manager.tack.SensorCreateModifyDTO;
import com.green.model.Sensor;
import com.green.model.Tree;

@Component
public class SensorConverter {
	public Sensor toSensor(SensorCreateModifyDTO dto) {
	
		Tree tree = Tree.builder()
				.treeId(dto.getTreeId())
				.build();
		
		return Sensor.builder()
				.tree(tree)
				.locationSensor(dto.getLocationSensor())
				.sensorName(dto.getSensorName())
				.samplingSpeed(dto.getSamplingSpeed())       
				.build();
	}
	
	public SensorCreateModifyDTO toSensorResponseDTO(Sensor sensor) {
		SensorCreateModifyDTO dto = SensorCreateModifyDTO
				.builder()
				.sensorId(sensor.getSensorId() != null ? sensor.getSensorId() : null)
				.treeId(sensor.getTree().getTreeId())
				.locationSensor(sensor.getLocationSensor())
				.sensorName(sensor.getSensorName())
				.samplingSpeed(sensor.getSamplingSpeed())
				.build();
		return dto;
	}

	public Sensor toSensor(SensorCreateModifyDTO dto, final Sensor sensor) {
		
		Tree tree = Tree.builder()
				.treeId(dto.getTreeId()).build();
		sensor.setTree(tree);
		sensor.setLocationSensor(dto.getLocationSensor());
		sensor.setSensorName(dto.getSensorName());
		sensor.setSamplingSpeed(dto.getSamplingSpeed());
		System.out.println("to update");
		return sensor;
	}
}