package com.green.manager.converter;

import org.springframework.stereotype.Component;

import com.green.manager.tack.UserDTO;
import com.green.model.User;

@Component
public class UserConverter {

	public User toUser(UserDTO dto) {

		User user = User.builder().userId(dto.getUserId()).build();

		return User.builder()
				.username(dto.getUsername()).password(dto.getPassword()).address(dto.getAddress()).phone(dto.getPhone())
				.email(dto.getEmail()).build();

	}

	public UserDTO toUserResponseDTO(User user) {
		UserDTO dto = UserDTO.builder().userId(user.getUserId() != null ? user.getUserId() : null)
				.username(user.getUsername()).password(user.getPassword()).address(user.getAddress())
				.phone(user.getPhone()).email(user.getEmail()).build();
		return dto;

	}

	public User toUser(UserDTO dto, final User user) {
		user.setUsername(dto.getUsername());
		user.setPassword(dto.getPassword());
		user.setAddress(dto.getAddress());
		user.setPhone(dto.getAddress());
		user.setEmail(dto.getEmail());
		System.out.println("to update");
		return user;
	}
}