package com.green.manager.converter;

import org.springframework.stereotype.Component;

import com.green.manager.tack.TreeDTO;
import com.green.model.Tree;

@Component
public class TreeConverter {
	public Tree toTree(TreeDTO dto) {

		Tree tree = Tree.builder().treeId(dto.getTreeId()).build();

		return Tree.builder().treeName(dto.getTreeName()).treeLocation(dto.getTreeLocation())
				.treeArea(dto.getTreeArea()).treeStatus(dto.getTreeStatus()).build();
	}

	public TreeDTO toTreeResponseDTO(Tree tree) {
		TreeDTO dto = TreeDTO.builder().treeId(tree.getTreeId()).treeLocation(tree.getTreeLocation())
				.treeName(tree.getTreeName()).treeArea(tree.getTreeArea()).treeStatus(tree.getTreeStatus()).build();
		return dto;

	}

	public Tree toTree(TreeDTO dto, final Tree tree) {
		tree.setTreeName(dto.getTreeName());
		tree.setTreeLocation(dto.getTreeLocation());
		tree.setTreeArea(dto.getTreeArea());
		tree.setTreeStatus(dto.getTreeStatus());
		System.out.println("to update");
		return tree;
	}
}
