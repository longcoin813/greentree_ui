package com.green.manager.converter;

import org.springframework.stereotype.Component;

import com.green.manager.tack.SensorDetailCreateModifyDTO ;
import com.green.model.Sensor;
import com.green.model.SensorDetail;

@Component
public class SensorDetailConverter {
	public SensorDetail toSensorDetail(SensorDetailCreateModifyDTO dto) {
	
		Sensor sensor = Sensor.builder()
				.sensorId(dto.getSensorId())
				.build();
		return SensorDetail.builder()
				.sensor(sensor)
				.day(dto.getDay())
				.status(dto.getStatus())
				.tilt(dto.getTilt())
				.tiltAngleThreshold(dto.getTiltAngleThreshold())
				.inclinationDirection(dto.getInclinationDirection())
				.batteryVoltage(dto.getBatteryVoltage())
				.temperature(dto.getTemperature())
				.humidity(dto.getHumidity())
				.co2(dto.getCo2())
				.build();
	}
	
	public SensorDetailCreateModifyDTO  toSensorDetailResponseDTO(SensorDetail sensorDetail) {
		SensorDetailCreateModifyDTO  dto = SensorDetailCreateModifyDTO 
				.builder()
				.sensorDetailId(sensorDetail.getSensorDetailId() != null ? sensorDetail.getSensorDetailId(): null)
				.sensorId(sensorDetail.getSensor().getSensorId())
				.day(sensorDetail.getDay())
				.status(sensorDetail.getStatus())
				.tilt(sensorDetail.getTilt())
				.tiltAngleThreshold(sensorDetail.getTiltAngleThreshold())
				.inclinationDirection(sensorDetail.getInclinationDirection())
				.batteryVoltage(sensorDetail.getBatteryVoltage())
				.temperature(sensorDetail.getTemperature())
				.humidity(sensorDetail.getHumidity())
				.co2(sensorDetail.getCo2())
				.build();
		System.out.println("Sensor detail converter: " + dto);
		return dto;
	}

	public SensorDetail toSensorDetail(SensorDetailCreateModifyDTO  dto, final SensorDetail sensorDetail) {
		
		Sensor sensor = Sensor.builder()
				.sensorId(dto.getSensorId())
				.build();
		sensorDetail.setSensor(sensor);
		sensorDetail.setDay(dto.getDay());
		sensorDetail.setStatus(dto.getStatus());
		sensorDetail.setTilt(dto.getTilt());
		sensorDetail.setTiltAngleThreshold(dto.getTiltAngleThreshold());
		sensorDetail.setInclinationDirection(dto.getInclinationDirection());
		sensorDetail.setBatteryVoltage(dto.getBatteryVoltage());
		sensorDetail.setTemperature(dto.getTemperature());
		sensorDetail.setHumidity(dto.getHumidity());
		sensorDetail.setCo2(dto.getCo2());
		return sensorDetail;
	}
}