package com.green.manager.converter;

import org.springframework.stereotype.Component;

import com.green.manager.tack.TreeDetailCreateModifyDTO;
import com.green.model.Tree;
import com.green.model.TreeDetail;

@Component
public class TreeDetailConverter {
	public TreeDetail toTreeDetail(TreeDetailCreateModifyDTO dto) {

		Tree tree = Tree.builder().treeId(dto.getTreeId()).build();
		System.out.println("Tree converter: " + tree);
		return TreeDetail.builder()
				.tree(tree)
				.treeAge(dto.getTreeAge())
				.ground(dto.getGround())
				.treeStatus(dto.getTreeStatus())
				.treeCareTimes(dto.getTreeCareTimes()).build();
	}

	public TreeDetailCreateModifyDTO toTreeDetailResponseDTO(TreeDetail tree) {
		TreeDetailCreateModifyDTO dto = TreeDetailCreateModifyDTO.builder()
				.treeDetailId(tree.getTreeDetailId() != null ? tree.getTreeDetailId() : null)
				.treeId(tree.getTree().getTreeId())
				.ground(tree.getGround())
				.treeAge(tree.getTreeAge()).treeStatus(tree.getTreeStatus())
				.treeCareTimes(tree.getTreeCareTimes()).build();
		System.out.println("Tree detail converter: " + dto);
		return dto;

	}

	public TreeDetail toTreeDetail(TreeDetailCreateModifyDTO dto, final TreeDetail treeDetail) {

		Tree tree = Tree.builder().treeId(dto.getTreeId()).build();
		treeDetail.setTree(tree);
		treeDetail.setTreeAge(dto.getTreeAge());
		treeDetail.setGround(dto.getGround());
		treeDetail.setTreeStatus(dto.getTreeStatus());
		treeDetail.setTreeCareTimes(dto.getTreeCareTimes());
		return treeDetail;
	}
}