package com.green.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = { "com.green.dao" })
@EnableTransactionManagement // anotation handles the opening and closing of transactions, such as commit,
								// rollback, close obj
public class JPAConfiguration {

	@Bean // where the entity manager factory is created, when we have entity manager we
			// can execute the sql queries
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() { // Support open connection for connection to
																			// query db
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource()); // GET CONNECTION - Where to provide Url, Driver, UserName, Password .....
		// em.setPackagesToScan(new String[] { "com.green.model" });
		em.setPersistenceUnitName("persistence-data"); // It is the bridge to get NewEntity and the new table in the
														// database
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());// put properties for JPA
		System.out.println("1");
		return em;
	}

	@Bean
	JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		System.out.println("2");
		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		System.out.println("3");
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		System.out.println("================="+dataSource);
		dataSource.setUrl("jdbc:mysql://us-cdbr-east-02.cleardb.com/heroku_1a28d62dc765ae3");
		System.out.println("4.1"+dataSource);
		dataSource.setUsername("b2f6205c6713b7");
		dataSource.setPassword("95323b21");
		System.out.println("4");
		return dataSource;
	}

	Properties additionalProperties() {
		Properties properties = new Properties();
		// properties.setProperty("hibernate.hbm2ddl.auto", "create-drop"); // Create db
		// from java entity class when we not have db, create db when we run and drop
		// when we turn off server
		properties.setProperty("hibernate.hbm2ddl.auto", "none");
//		properties.setProperty("hibernate.hbm2ddl.auto", "create");
		properties.setProperty("hibernate.enable_lazy_load_no_trans", "true");
		System.out.println("5");
		return properties;
	}
}
